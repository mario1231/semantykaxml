import javax.xml.bind.annotation.XmlAttribute;

public class Transition {

    public Transition() {
        this.from = this.to = this.require = 0;
    }

    public Transition(int from, int to, int require) {
        this.from = from;
        this.to = to;
        this.require = require;
    }

    public float getFrom() {
        return from;
    }

    public float getTo() {
        return to;
    }

    public float getRequire() {
        return require;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t\t" + this.from + "_" + this.to + "_" + this.require + "\n");

        return stringBuilder.toString();
    }

    @XmlAttribute(name="from")
    public int from;
    @XmlAttribute(name="to")
    public int to;
    @XmlAttribute(name="require")
    public int require;
}
