import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="network")
public class Network {
    @XmlElements(@XmlElement(name="automaton"))
    public List<Automaton> automaton;

    Network() {
        automaton = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("NETWORK\n");

        for (Automaton a : automaton) {
            stringBuilder.append(a.toString());
        }

        stringBuilder.append("END\n");

        return stringBuilder.toString();
    }
}
