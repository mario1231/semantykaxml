import com.sun.tools.javac.util.Convert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLNetworkParser {
    public static Network XMLToNetwork(String filename) {
        Network network = null;

        try {
            File f = new File(filename);
            JAXBContext jaxbContext = JAXBContext.newInstance(Network.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            network = (Network)unmarshaller.unmarshal(f);
        }
        catch (JAXBException ex) {
            ex.printStackTrace();
        }

        return network;
    }

    public static void NetworkToXML(Network network, String filename) {
        if (network == null)
            return;

        try {
            File file = new File(filename);
            JAXBContext jaxbContext = JAXBContext.newInstance(Network.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(network, file);
        }
        catch (JAXBException ex) {
            ex.printStackTrace();
        }
    }

    public static void XMLToFormat(String xmlFilename, String formatFilename) {
        File format = new File(formatFilename);
        Network n = XMLToNetwork(xmlFilename);

        try (FileWriter out = new FileWriter(format)) {
            out.write(n.toString());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void FormatToXML(String formatFilename, String xmlFilename) {
        File format = new File(formatFilename);
        Network network = new Network();

        String networkPatternString = "(NETWORK)";
        String automatonPatternString = "(\\s*)(AUTOMATON_)(\\d+)";
        String locationPatternString = "((\\s*)LOCATION\\[)(\\d+)(\\:)(\\d+)(\\])";
        String transitionsPatternString = "(\\s*)(TRANSITIONS)";
        String transitionPatternString = "(\\s*)(\\d+)(_)(\\d+)(_)(\\d+)";

        Pattern automatonPattern = Pattern.compile(automatonPatternString);
        Pattern locationPattern = Pattern.compile(locationPatternString);
        Pattern transitionPattern = Pattern.compile(transitionPatternString);

        Matcher matcher = null;

        try (BufferedReader in = new BufferedReader(new FileReader(formatFilename))) {
            String line;
            line = in.readLine();

            //NETWORK
            if (line == null || !line.matches(networkPatternString))
                throw new IOException();

            //  AUTOMATA
            while((line = in.readLine()) != null && !line.contains("END")) {
                Automaton automaton = new Automaton();

                //  AUTOMATON
                if (!line.matches(automatonPatternString))
                    throw new IOException();

                matcher = automatonPattern.matcher(line);
                if (matcher.find())
                    automaton.id = Integer.parseInt(matcher.group(3));

                //      LOCATION[X:Y]
                line = in.readLine();
                if (!line.matches(locationPatternString))
                    throw new IOException();

                matcher = locationPattern.matcher(line);
                if (matcher.find()) {
                    automaton.location_from = Integer.parseInt(matcher.group(3));
                    automaton.location_to = Integer.parseInt(matcher.group(5));
                    automaton.state = java.sql.Types.NULL;
                }

                //      TRANSITIONS
                line = in.readLine();
                if (!line.matches(transitionsPatternString))
                    throw new IOException();

                while((line = in.readLine()) != null && !line.contains("END")) {
                    //      X_Y_Z
                    Transition transition = new Transition();

                    if (!line.matches(transitionPatternString)) {
                        System.out.println(line);
                        throw new IOException();
                    }

                    matcher = transitionPattern.matcher(line);
                    if (matcher.find()) {
                        transition.from = Integer.parseInt(matcher.group(2));
                        transition.to = Integer.parseInt(matcher.group(4));
                        transition.require = Integer.parseInt(matcher.group(6));
                    }

                    automaton.transition.add(transition);
                }

                network.automaton.add(automaton);
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

        NetworkToXML(network, xmlFilename);
    }
}
