import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Types.NULL;

public class Automaton {
    public Automaton() {
        this.id = 0;
        this.location_from = 0;
        this.location_to = 0;
        this.state = 0;
        this.transition = new ArrayList<>();
    }

    public Automaton(int id, int location_from, int location_to, int state, List<Transition> transitions) {
        this.state = state == NULL ? location_from : state;
        this.id = id;
        this.location_from = location_from;
        this.location_to = location_to;
        this.transition = transitions;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\tAUTOMATON_" + this.id + "\n\t\tLOCATION[" + this.location_from + ":" + this.location_to + "]\n\t\tTRANSITIONS\n");

        for (Transition t : transition) {
            stringBuilder.append(t.toString());
        }

        stringBuilder.append("\tEND\n");

        return stringBuilder.toString();
    }

    @XmlAttribute(name="id")
    public int id;
    @XmlAttribute(name="location_from")
    public int location_from;
    @XmlAttribute(name="location_to")
    public int location_to;
    @XmlAttribute
    public int state;
    @XmlElements(@XmlElement(name="transition"))
    List<Transition> transition;

}
